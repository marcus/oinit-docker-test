#!/bin/bash
set -e

# make sure we're on the latest version of oinit
PACKAGE="oinit"
apt-get update > /dev/null
apt-get -y install ${PACKAGE} > /dev/null
apt-cache policy ${PACKAGE} | \
    grep Installed | sed s/Installed/${PACKAGE}/

exec "$@"
