#################### ca-server server ####################################
FROM marcvs/build_generic_debian-bookworm AS oinit-ca

RUN curl repo.data.kit.edu/devel/automatic-repo-data-kit-edu-key.gpg \
        | gpg --dearmor \
        > /etc/apt/trusted.gpg.d/auto-kitrepo-archive.gpg
RUN echo 'deb [signed-by=/etc/apt/trusted.gpg.d/auto-kitrepo-archive.gpg] https://repo.data.kit.edu/devel/debian/bookworm ./' > /etc/apt/sources.list.d/kit-repo.list
RUN apt-get update
RUN apt-get -y install netcat-openbsd iproute2 net-tools iputils-ping telnet procps neovim
# install and setup oinit-ca
RUN apt-get -y install oinit-ca
RUN rm -f /etc/oinit-ca/user-ca*
RUN rm -f /etc/oinit-ca/host-ca*
RUN /usr/bin/ssh-keygen -t ed25519 -f /etc/oinit-ca/user-ca -N ""
RUN /usr/bin/ssh-keygen -t ed25519 -f /etc/oinit-ca/host-ca -N ""
RUN ls -l /etc/oinit-ca/user-ca.pub
# a hack: we generate the hostkey of the ssh-server on the ca, because we
# can only copy files from oinit-ca to ssh-server during `docker build`
RUN /usr/bin/ssh-keygen -t ed25519 -f /tmp/host-key -N ""
RUN /usr/bin/ssh-keygen -s /etc/oinit-ca/host-ca \
                        -I ssh-server \
                        -h \
                        -n ssh-server,hpc.example.org /tmp/host-key.pub


COPY etc/oinit-ca/config.ini /etc/oinit-ca/config.ini
COPY ./oinit-ca/entrypoint.sh /srv/entrypoint.sh
RUN chmod 755 /srv/entrypoint.sh
ENTRYPOINT [ "/srv/entrypoint.sh" ]


#################### ssh client ####################################
FROM marcvs/build_generic_debian-bookworm AS ssh-client

RUN curl repo.data.kit.edu/devel/automatic-repo-data-kit-edu-key.gpg \
        | gpg --dearmor \
        > /etc/apt/trusted.gpg.d/auto-kitrepo-archive.gpg
RUN echo 'deb [signed-by=/etc/apt/trusted.gpg.d/auto-kitrepo-archive.gpg] https://repo.data.kit.edu/devel/debian/bookworm ./' > /etc/apt/sources.list.d/kit-repo.list
RUN apt-get update
RUN apt-get -y install netcat-openbsd iproute2 net-tools iputils-ping telnet procps neovim
RUN apt-get -y install --no-install-recommends oinit
# setup oinit-ca
COPY --from=oinit-ca /etc/oinit-ca/host-ca.pub /etc/ssh/

RUN apt-get -y install oidc-agent-cli
RUN useradd -s /bin/bash -m user

COPY ./ssh-client/entrypoint.sh /srv/entrypoint.sh
RUN chmod 755 /srv/entrypoint.sh
ENTRYPOINT [ "/srv/entrypoint.sh" ]


#################### motley-cue ####################################
FROM debian:bookworm AS ssh-server

##### install dependencies
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    ssh \
    curl \
    gpg
RUN echo deb [signed-by=/etc/apt/trusted.gpg.d/kitrepo-archive.gpg] https://repo.data.kit.edu/devel/debian/bookworm ./ \
    >> /etc/apt/sources.list
RUN curl repo.data.kit.edu/devel/automatic-repo-data-kit-edu-key.gpg \
    | gpg --dearmor \
    > /etc/apt/trusted.gpg.d/kitrepo-archive.gpg
RUN apt-get update && apt-get install -y \
    motley-cue

##### tools
RUN apt-get -y install netcat-openbsd iproute2 net-tools iputils-ping telnet procps neovim

##### ssh config
RUN mkdir /run/sshd
RUN echo "Include /etc/ssh/sshd_config.d/*.conf" >> /etc/ssh/sshd_config \
    && echo "ChallengeResponseAuthentication yes" > /etc/ssh/sshd_config.d/oidc.conf

##### pam config
# pam-ssh optional
RUN apt-get install -y pam-ssh-oidc-autoconfig 
RUN sed -i "s/localhost/nginx.hpc.example.org/g" /etc/pam.d/pam-ssh-oidc-config.ini

##### motley-cue config
RUN rm /etc/motley_cue/motley_cue.conf  && rm /etc/motley_cue/feudal_adapter.conf \
    && ln -s /config_files/motley_cue.conf /etc/motley_cue/motley_cue.conf \
    && ln -s /config_files/feudal_adapter.conf /etc/motley_cue/feudal_adapter.conf
RUN echo ERROR_LOG=- >> /etc/motley_cue/motley_cue.env
RUN echo ACCESS_LOG=- >> /etc/motley_cue/motley_cue.env

# ##### expose needed ports
# EXPOSE 22

##### init cmd and entrypoint
COPY ./ssh-server/runner.sh /srv/runner.sh
COPY ./ssh-server/entrypoint.sh /srv/entrypoint.sh
RUN chmod +x /srv/runner.sh /srv/entrypoint.sh

########
# ssh
RUN apt-get -y install oinit-openssh

# setup oinit
COPY --from=oinit-ca /etc/oinit-ca/user-ca.pub /etc/ssh/
COPY --from=oinit-ca /tmp/host-key* /etc/ssh/
RUN echo "HostKey /etc/ssh/host-key" >> /etc/ssh/sshd_config
RUN echo "HostCertificate /etc/ssh/host-key-cert.pub" >> /etc/ssh/sshd_config
RUN echo "TrustedUserCAKeys /etc/ssh/user-ca.pub" >> /etc/ssh/sshd_config
RUN echo "Match User oinit" >> /etc/ssh/sshd_config.d/oidc
RUN echo "    PasswordAuthentication no" >> /etc/ssh/sshd_config.d/oidc

# FIXME: put these two lines on top of /etc/pam.d/su
RUN echo "auth [success=ignore default=1] pam_succeed_if.so use_uid user = oinit" >>/tmp/su
RUN echo "auth sufficient                 pam_succeed_if.so uid ne 0 " >>/tmp/su
RUN cat /etc/pam.d/su >> /tmp/su
RUN cat /tmp/su > /etc/pam.d/su
# /ssh

ENTRYPOINT [ "/srv/entrypoint.sh" ]
CMD ["/srv/runner.sh"]



#################### nginx ####################################
FROM nginx:alpine AS nginx
COPY motley-cue/nginx.motley_cue /etc/nginx/conf.d/default.conf
EXPOSE 8080
