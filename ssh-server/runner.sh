#!/bin/bash
set -e

echo "running sshd" >> /tmp/mylog
/usr/sbin/sshd &
echo "done running sshd" >> /tmp/mylog
echo "running motley-cue" >> /tmp/mylog
/usr/sbin/motley-cue
echo "done running motley-cue" >> /tmp/mylog
# /usr/sbin/motley-cue &
# /usr/sbin/sshd -D -e
wait -n
