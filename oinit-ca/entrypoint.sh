#!/bin/bash
set -e

## copy default files if config folder is empty
[ -d /config_files ] && {
    rm -f /etc/oinit-ca/config.ini
    cp /config_files/config.ini /etc/oinit-ca/
}
[ -d /config_files ] || {
    echo "========== NOT copying config files =========="
}
#
# make sure we're on the latest version of oinit
PACKAGE="oinit-ca"
apt-get update 
apt-get -y install ${PACKAGE} 
apt-cache policy ${PACKAGE} | \
    grep Installed | sed s/Installed/${PACKAGE}/

exec "$@"
