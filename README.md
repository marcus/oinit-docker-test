# oinit demo docker

## Starting it

These commands _should_ start the whole environment:
```
docker compose build
docker compose up
```

Watch out for problems with alread-used ports! This may lead to individual
containers failing to come `up`.


## What's inside?

After start `docker ps` should show (at least) these containers:

- `oinit-docker-test-oinit-ca`: the `oinit-ca` server, listening on port 8080
- `oinit-docker-test-nginx`: the `nginx` frontend for `motley-cue`
- `oinit-docker-test-ssh-client`: the `ssh` client for our demo
- `oinit-docker-test-ssh-sever`: `motley-cue`, `oinit-openssh` and `ssh` server for our demo


## Configuration for the demo

1. Adapt `etc/motley_cue/motley_cue.conf` to ensure your own user will be
   authorised
1. (Only if you are already using `oidc-agent`): Copy an oidc-agent configuration file to
   `userhome/.config/oic-agent/`


## Using the Demo

1. Get into the demo ssh client machine:
```
docker exec -it oinit-docker-test-ssh-client-1 bash
```
2. Setup the environment:
```
su - user
eval `ssh-agent` # we store the ssh-certificate here
export OIDC=<YOUR ACCESS TOKEN HERE>
```
3. Setup ssh to use oinit-ca for `hpc.example.org`:
```
oinit add hpc.example.org http://oinit-ca.hpc.example.org:8080
```
4. ssh into the example host:
```
ssh hpc.example.org
```

## Using the Demo with oidc-agent

Instead of the above `export OIDC` line, run these steps:

1. Outside of the cointainer: Copy an existing configuration of your laptop to the $HOME of the
   demo user:
```
mkdir -p userhome/.config/oidc-agent
cp ~/.config/oidc-agent/<name of your config> userhome/.config/oidc-agent
```
2. Inside the demo ssh client container: start oidc-agent, and load a
   config
```
eval `oidc-agent-service start`
oidc-add <name of your config>
```

# `pam-ssh-oidc`
## Use ssh via `pam-ssh-oidc` and `mccli` (without certificates):

```
pip install mccli
.local/bin/mccli ssh hpc.example.org --oidc egi --mc-endpoint http://nginx.hpc.example.org:8080
```


## Tricks

Tricks that we used to avoid caveats:

- `etc/motley_cue/feudal_adapter.conf`: We have defined a default primary
    group. The one derived from user attributes might be too long and then
    cause errors.
- user/.bash_history contains useful commands to run the demo


eval `ssh-agent`; eval `oidc-agent-service start`; oidc-add egi; oidc-add google
